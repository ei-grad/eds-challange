import os

from pyspark.sql import SparkSession, functions as F


spark = (
    SparkSession.builder
    .getOrCreate()
)

num_partitions = 10

src = os.getenv("SRCDIR", ".")
dst = os.getenv("DSTDIR", ".")


def to_rub(col):
    return F.col(col).cast("decimal(10,2)")


def to_int(col):
    return F.col(col).cast("int")


# ch.xackaton_bitovie_abon_nach_dz_kz_oplata_realisatsiya.csv
b2c_billing = (
    spark.read
    .csv(f"{src}/csv/b2c/billing.csv", header=True, escape='"')
    .select(
        "month_year",
        to_int("id"),
        to_rub("dz_for_period_begin_rub"),
        to_rub("kz_for_period_begin_rub"),
        to_int("charged_total_kVth"),
        to_rub("charged_total_rub"),
        to_rub("user_pay_rub"),
        to_rub("realization_rub"),
        to_rub("current_year_realization_rub"),
        to_rub("current_period_realization_rub"),
        to_rub("dz_for_period_end_rub"),
        to_rub("kz_for_period_end_rub"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id", "month_year")
)
if not os.path.exists(f"{dst}/parquet/b2c/billing"):
    b2c_billing.write.partitionBy("month_year").parquet(f"{dst}/parquet/b2c/billing")

# ch.xackaton_fuv2_bitovie_abon_parametri_dogovora.csv
b2c_accounts = (
    spark.read
    .csv(f"{src}/csv/b2c/accounts.csv", header=True, escape='"')
    .select(
        to_int("id"),
        "ownerhip_form",
        "coliving_flag",
        "home_running_way",
        "type_ls_building",
        "counter_type",
        "counter_category",
        "pu_setup_place",
        "pu_actual_state",
        F.col("odn_is_valuable").cast("boolean"),
        F.col("calling_is_on").cast("boolean"),
        F.col("autocalling_is_on").cast("boolean"),
        "account_type",
        "epd_sign",
        "epd_sign_1",
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2c/accounts"):
    b2c_accounts.write.parquet(f"{dst}/parquet/b2c/accounts")

# ch.xackaton_po_bitovie_abon_nachisleniya_2019_2021.csv
b2c_consumption = (
    spark.read
    .csv(f"{src}/csv/b2c/consumption.csv", header=True, escape='"')
    .select(
        "day_period",
        to_int("id"),
        F.to_date("month_year", "yyyy.MM").alias("month_year"),
        "stove_type",
        "tariff",
        "voltage",
        to_int("used_energy_capacity_kVth"),
        to_rub("used_energy_cost_with_nds_rub"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id", "month_year")
)
if not os.path.exists(f"{dst}/parquet/b2c/consumption"):
    b2c_consumption.write.parquet(f"{dst}/parquet/b2c/consumption")

# ch.xackaton_suvk_bitovie_abon_parametri.csv
b2c_svc_status = (
    spark.read.csv(f"{src}/csv/b2c/svc_status.csv", header=True, escape='"')
    .select(
        F.to_date("dt").alias("dt"),
        to_int("id"),
        "request_status",
        F.col("dtime").cast("timestamp"),
        F.col("closing_date").cast("timestamp"),  # XXX: should it be date? it contains some values with hour/minute
        "request_number",
        "request_type",
        "pay_type",
        "finance_status",
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id", "dtime")
)
if not os.path.exists(f"{dst}/parquet/b2c/svc_status"):
    b2c_svc_status.write.parquet(f"{dst}/parquet/b2c/svc_status")

# ch.xackaton_suvk_bitovie_abon_platnie_nachisleniya_i_oplati.csv
b2c_svc_payments = (
    spark.read
    .csv(f"{src}/csv/b2c/svc_payments.csv", header=True, escape='"')
    .select(
        F.to_date("dt").alias("dt"),
        to_int("id"),
        "status",
        F.col("dtime").cast("timestamp"),
        F.col("closing_date").cast("timestamp"),  # XXX: should it be date? it contains some values with hour/minute
        "request_number",
        "service_name",
        F.col("end_work_dtime").cast("timestamp"),
        to_rub("unpaid_sum_with_nds"),
        to_rub("services_sum_cost_with_nds"),
        to_rub("pay_sum_with_nds"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id", "dtime")
)
if not os.path.exists(f"{dst}/parquet/b2c/svc_payments"):
    b2c_svc_payments.write.parquet(f"{dst}/parquet/b2c/svc_payments")

# ch.xackaton_suvk_bitovie_abon_obrascheniya_2019_2021.csv
b2c_svc_tickets = (
    spark.read
    .csv(f"{src}/csv/b2c/svc_tickets.csv", header=True, escape='"')
    .select(
        F.to_date("dt").alias("dt"),
        to_int("id"),
        "request_status",
        "request_type",
        "topic_level_1",
        "topic_level_2",
        "topic_level_3",
        "task_priority",
        "task_status",
        "task_type",
        "work_done",
        "topic_attribute",
        F.regexp_extract("task_number", "\\d+", 0).cast("int").alias("task_number"),
        F.regexp_extract("request_number", "\\d+", 0).cast("int").alias("request_number"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id", "dt")
)
if not os.path.exists(f"{dst}/parquet/b2c/svc_tickets"):
    b2c_svc_tickets.write.parquet(f"{dst}/parquet/b2c/svc_tickets")


######################
# B2B
######################

b2b_1 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_fuv2_parametri_dogovora_yur_nds_mkzd_odn_finsource.csv",
         header=True, escape='"')
    .select(
        to_int("id"),
        F.col('nds_is_valuable').cast("boolean"),
        to_int('payment_date'),  # платежный день
        'finance_source',
        F.col('mkzd_is_valuable').cast("boolean"),
        F.col('odn_is_valuable').cast("boolean"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/fuv2_parametri_dogovora_yur_nds_mkzd_odn_finsource"):
    b2b_1.write.parquet(f"{dst}/parquet/b2b/fuv2_parametri_dogovora_yur_nds_mkzd_odn_finsource")


b2b_2 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_fuv2_pararmetri_avansovih_raschetov.csv",
         header=True, escape='"')
    .select(
        to_int("id"),
        to_int("prepayment_date_1"),
        to_int("prepayment_date_2"),
        to_int("prepayment_percent_1"),
        to_int("prepayment_percent_2"),
        to_int("fake_id"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/fuv2_pararmetri_avansovih_raschetov"):
    b2b_2.write.parquet(f"{dst}/parquet/b2b/fuv2_pararmetri_avansovih_raschetov")


b2b_3 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_fuv2_yur_litsa_oplati_prom.csv",
         header=True, escape='"')
    .select(
        'month_year',
        to_int("id"),
        to_rub('paid_with_nds_rub'),
        to_rub('paid_to_current_usage_with_nds_rub'),
        to_rub('paid_as_debt_with_nds_rub'),
        to_rub('paid_as_prepayment_with_nds_rub'),
        to_rub('total_realiztion_with_nds_rub'),
        to_rub('by_acts_of_ex_usage_with_nds'),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/fuv2_yur_litsa_oplati_prom"):
    b2b_3.write.partitionBy("month_year").parquet(f"{dst}/parquet/b2b/fuv2_yur_litsa_oplati_prom")


b2b_4 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_fuv2_yur_nachisleniya_i_oplati_avansov.csv",
         header=True, escape='"')
    .select(
        'month_year',
        to_int("id"),
        F.regexp_extract(
            "prepayment_percent", "[0-9\\.]+", 0
        ).cast("float").alias('prepayment_percent'),
        to_rub('prepayments_charged_with_nds_rub'),
        to_rub('prepayments_paid_with_nds_rub'),
        'charge_type_prom',
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/fuv2_yur_nachisleniya_i_oplati_avansov"):
    b2b_4.write.partitionBy("month_year").parquet(f"{dst}/parquet/b2b/fuv2_yur_nachisleniya_i_oplati_avansov")


b2b_5 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_fuv2_yur_nachisleniya_oplati_dz_kz_2019_2021.csv",
         header=True, escape='"')
    .select(
        'month_year',
        to_int("id"),
        'contract_kind',
        'contract_type',
        'msfo_group_name',
        to_int('useful_vacation_kVth'),
        to_rub('useful_vacation_total_rub'),
        to_rub('charge_cost_total_rub'),
        to_rub('realization_total_rub'),
        to_rub('dz_for_period_end_with_nds_rub'),
        to_rub('kz_for_period_end_with_nds_rub'),
        to_rub('total_kz_for_period_end_nds_rub'),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/fuv2_yur_nachisleniya_oplati_dz_kz"):
    b2b_5.write.partitionBy("month_year").parquet(f"{dst}/parquet/b2b/fuv2_yur_nachisleniya_oplati_dz_kz")


b2b_6 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_fuv2_yur_nachisleniya_prom_ee.csv",
         header=True, escape='"')
    .select(
        'month_year',
        to_int("id"),
        'price_category',
        to_int('useful_vacation_kVt'),
        to_rub('useful_vacation_with_nds_rub'),
        to_rub('charged_by_acts_ex_usage_with_nds_rub'),
        'product_type',
        'power_range',
        'calc_and_charging_algorithm',
        'prom_charging_type',
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/fuv2_yur_nachisleniya_prom_ee"):
    b2b_6.write.partitionBy("month_year").parquet(f"{dst}/parquet/b2b/fuv2_yur_nachisleniya_prom_ee")


b2b_7 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_po_yur_dannie_po_dogovoru.csv",
         header=True, escape='"')
    .select(
        to_int("id"),
        "finance_source_name",
        "msfo_group_name",
        F.regexp_extract("nds_is_valuable", "[01]", 0).cast("int").cast("boolean").alias("nds_is_valuable"),
        "okved_name",
        "voltage",
        "user_subgroup",
        "eso_is_valuable",
        "contract_type"
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/po_yur_dannie_po_dogovoru"):
    b2b_7.write.parquet(f"{dst}/parquet/b2b/po_yur_dannie_po_dogovoru")


b2b_8 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_po_yur_litsa_nachisleniya_2019_2021.csv",
         header=True, escape='"')
    .select(
        "month_year",
        to_int("id"),
        "price_category",
        "max_power_range",
        "calc_algorithm",
        to_rub("cost_of_taken_energy_with_nds_rub"),
        to_int("unaccounted_energy_usage_kVt"),
        to_rub("unaccounted_energy_usage_rub"),
        to_int("power_capacity_kVt"),
        to_rub("power_capacity_with_nds_rub"),
        to_int("unaccounted_power_usage_kVt"),
        to_rub("unaccounted_power_usage_rub"),
        to_rub("ore_price_with_nds_rub"),
        to_int("used_energy_capacity_kVth"),
        "voltage",
        to_int("ore_price_kVt"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/po_yur_litsa_nachisleniya"):
    b2b_8.write.partitionBy("month_year").parquet(f"{dst}/parquet/b2b/po_yur_litsa_nachisleniya")


b2b_9 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_po_yur_poleznii_otpusk_2019_2021.csv",
         header=True, escape='"')
    .select(
        "month_year",
        F.col("registration_date").cast("date"),
        to_int("hour"),
        to_int("id"),
        "price_category",
        "calc_algorithm",
        "voltage",
        F.col("energy_taken_kVth").cast("float"),
        F.col("cost_of_taken_energy_with_nds_rub").cast("float"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/po_yur_poleznii_otpusk"):
    b2b_9.write.partitionBy("month_year").parquet(f"{dst}/parquet/b2b/po_yur_poleznii_otpusk")


b2b_10 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_suvk_yur_obrascheniya.csv",
         header=True, escape='"')
    .select(
        F.to_date("dt").alias("dt"),
        to_int("id"),
        "request_status",
        "request_type",
        "topic_level_1",
        "topic_level_2",
        "topic_level_3",
        "task_priority",
        "task_status",
        "task_type",
        # XXX: differs from ch.xackaton_suvk_bitovie_abon_obrascheniya_2019_2021.csv:
        # missing the "work_done" column
        "topic_attribute",
        F.regexp_extract("task_number", "\\d+", 0).cast("int").alias("task_number"),
        F.regexp_extract("request_number", "\\d+", 0).cast("int").alias("request_number"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id", "dt")
)
if not os.path.exists(f"{dst}/parquet/b2b/suvk_yur_obrascheniya"):
    b2b_10.write.parquet(f"{dst}/parquet/b2b/suvk_yur_obrascheniya")

b2b_11 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_suvk_yur_platnie_nachisleniya_i_oplati.csv",
         header=True, escape='"')
    .select(
        F.col("dt").cast("date"),
        to_int("id"),
        "status",
        F.col("dtime").cast("timestamp"),
        F.col("closing_date").cast("timestamp"),
        "request_number",
        "service_name",
        F.translate("unpaid_sum_with_nds", ",", ".").cast("decimal(10,2)").alias("unpaid_sum_with_nds"),
        F.translate("pay_sum_with_nds", ",", ".").cast("decimal(10,2)").alias("pay_sum_with_nds"),
        F.translate("services_sum_cost_with_nds", ",", ".").cast("decimal(10,2)").alias("services_sum_cost_with_nds"),
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/suvk_yur_platnie_nachisleniya_i_oplati"):
    b2b_11.write.parquet(f"{dst}/parquet/b2b/suvk_yur_platnie_nachisleniya_i_oplati")

b2b_12 = (
    spark.read
    .csv(f"{src}/csv/b2b/ch.xackaton_suvk_yur_platnie_parametri_zayavki.csv",
         header=True, escape='"')
    .select(
        F.col('dt').cast("date"),
        to_int('id'),
        'status',
        F.col('dtime').cast("timestamp"),
        F.col('closing_date').cast("timestamp"),
        'request_number',
        'request_type',
        'pay_type',
        'finance_status'
    )
    .repartition(num_partitions, "id")
    .sortWithinPartitions("id")
)
if not os.path.exists(f"{dst}/parquet/b2b/suvk_yur_platnie_parametri_zayavki"):
    b2b_12.write.parquet(f"{dst}/parquet/b2b/suvk_yur_platnie_parametri_zayavki")
