CREATE OR REPLACE TABLE fuv2_parametri_dogovora_yur_nds_mkzd_odn_finsource
ENGINE MergeTree
ORDER BY id
AS SELECT * FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/fuv2_parametri_dogovora_yur_nds_mkzd_odn_finsource/*.parquet',
    'Parquet',
    'id UInt32, nds_is_valuable Boolean, payment_date UInt32, finance_source String, mkzd_is_valuable Boolean, odn_is_valuable Boolean'
);

CREATE OR REPLACE TABLE fuv2_pararmetri_avansovih_raschetov
ENGINE MergeTree
ORDER BY id
AS SELECT * FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/fuv2_pararmetri_avansovih_raschetov/*.parquet',
    'Parquet',
    'id UInt32, prepayment_date_1 Nullable(UInt32), prepayment_date_2 Nullable(UInt32), prepayment_percent_1 Nullable(UInt32), prepayment_percent_2 Nullable(UInt32), fake_id UInt32'
);

CREATE OR REPLACE TABLE fuv2_yur_litsa_oplati_prom
ENGINE MergeTree
ORDER BY id
PARTITION BY month_year
AS SELECT
    extract(_path, '/month_year=([^/]+)/') AS month_year,
    *
FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/fuv2_yur_litsa_oplati_prom/month_year=*/*.parquet',
    'Parquet',
    'id UInt32, paid_with_nds_rub Nullable(Decimal(10,2)), paid_to_current_usage_with_nds_rub Nullable(Decimal(10,2)), paid_as_debt_with_nds_rub Nullable(Decimal(10,2)), paid_as_prepayment_with_nds_rub Nullable(Decimal(10,2)), total_realiztion_with_nds_rub Nullable(Decimal(10,2)), by_acts_of_ex_usage_with_nds Nullable(Decimal(10,2))'
);


CREATE OR REPLACE TABLE fuv2_yur_nachisleniya_i_oplati_avansov
ENGINE MergeTree
ORDER BY id
PARTITION BY month_year
AS SELECT
    extract(_path, '/month_year=([^/]+)/') AS month_year,
    *
FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/fuv2_yur_nachisleniya_i_oplati_avansov/month_year=*/*.parquet',
    'Parquet',
    'id UInt32, prepayment_percent Float32, prepayments_charged_with_nds_rub Nullable(Decimal(10,2)), prepayments_paid_with_nds_rub Nullable(Decimal(10,2)), charge_type_prom String'
);


CREATE OR REPLACE TABLE fuv2_yur_nachisleniya_oplati_dz_kz
ENGINE MergeTree
ORDER BY id
PARTITION BY month_year
AS SELECT
    extract(_path, '/month_year=([^/]+)/') AS month_year,
    *
FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/fuv2_yur_nachisleniya_oplati_dz_kz/month_year=*/*.parquet',
    'Parquet',
    'id UInt32, contract_kind String, contract_type String, msfo_group_name String, useful_vacation_kVth Nullable(Int32), useful_vacation_total_rub Nullable(Decimal(10,2)), charge_cost_total_rub Nullable(Decimal(10,2)), realization_total_rub Nullable(Decimal(10,2)), dz_for_period_end_with_nds_rub Nullable(Decimal(10,2)), kz_for_period_end_with_nds_rub Nullable(Decimal(10,2)), total_kz_for_period_end_nds_rub Nullable(Decimal(10,2))'
);


CREATE OR REPLACE TABLE fuv2_yur_nachisleniya_prom_ee
ENGINE MergeTree
ORDER BY id
PARTITION BY month_year
AS SELECT
    extract(_path, '/month_year=([^/]+)/') AS month_year,
    *
FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/fuv2_yur_nachisleniya_prom_ee/month_year=*/*.parquet',
    'Parquet',
    'id UInt32, price_category String, useful_vacation_kVt Nullable(Int32), useful_vacation_with_nds_rub Nullable(Decimal(10,2)), charged_by_acts_ex_usage_with_nds_rub Nullable(Decimal(10,2)), product_type String, power_range Nullable(String), calc_and_charging_algorithm String, prom_charging_type String'
);


CREATE OR REPLACE TABLE po_yur_dannie_po_dogovoru
ENGINE MergeTree
ORDER BY id
AS SELECT * FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/po_yur_dannie_po_dogovoru/*.parquet',
    'Parquet',
    'id UInt32, finance_source_name String, msfo_group_name Nullable(String), nds_is_valuable Boolean, okved_name Nullable(String), voltage Nullable(String), user_subgroup Nullable(String), eso_is_valuable Nullable(String), contract_type Nullable(String)'
);


CREATE OR REPLACE TABLE po_yur_litsa_nachisleniya
ENGINE MergeTree
ORDER BY id
PARTITION BY month_year
AS SELECT
    extract(_path, '/month_year=([^/]+)/') AS month_year,
    *
FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/po_yur_litsa_nachisleniya/month_year=*/*.parquet',
    'Parquet',
    'id UInt32,
     price_category String,
     max_power_range Nullable(String),
     calc_algorithm Nullable(String),
     cost_of_taken_energy_with_nds_rub Nullable(Decimal(10,2)),
     unaccounted_energy_usage_kVt Int32,
     unaccounted_energy_usage_rub Decimal(10,2),
     power_capacity_kVt Int32,
     power_capacity_with_nds_rub Nullable(Decimal(10,2)),
     unaccounted_power_usage_kVt Int32,
     unaccounted_power_usage_rub Decimal(10,2),
     ore_price_with_nds_rub Nullable(Decimal(10,2)),
     used_energy_capacity_kVth Int32,
     voltage Nullable(String),
     ore_price_kVt Int32'
);


CREATE OR REPLACE TABLE suvk_yur_obrascheniya
ENGINE MergeTree
ORDER BY id
AS SELECT * FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/suvk_yur_obrascheniya/*.parquet',
    'Parquet',
    'dt Date,
     id UInt32,
     request_status String,
     request_type String,
     topic_level_1 String,
     topic_level_2 String,
     topic_level_3 String,
     task_priority String,
     task_status String,
     task_type String,
     topic_attribute String,
     task_number Nullable(Int32),
     request_number Int32'
);


CREATE OR REPLACE TABLE suvk_yur_platnie_nachisleniya_i_oplati
ENGINE MergeTree
ORDER BY id
AS SELECT * FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/suvk_yur_platnie_nachisleniya_i_oplati/*.parquet',
    'Parquet',
    'dt Date,
     id UInt32,
     status String,
     dtime Datetime,
     closing_date Nullable(Datetime),
     request_number String,
     service_name String,
     unpaid_sum_with_nds Nullable(Decimal(10,2)),
     pay_sum_with_nds Nullable(Decimal(10,2)),
     services_sum_cost_with_nds Decimal(10,2)'
);


CREATE OR REPLACE TABLE suvk_yur_platnie_parametri_zayavki
ENGINE MergeTree
ORDER BY id
AS SELECT * FROM s3(
    'https://storage.yandexcloud.net/energy-and-brains/suvk_yur_platnie_parametri_zayavki/*.parquet',
    'Parquet',
    'dt Date,
     id UInt32,
     status String,
     dtime Datetime,
     closing_date Nullable(Datetime),
     request_number String,
     request_type String,
     pay_type String,
     finance_status String'
);
