import base64

import streamlit as st

from ch_query import ch_query


def as_csv(df):
    csv = df.to_csv(index=False).encode()
    b64 = base64.b64encode(csv).decode()
    return (
        f'<a href="data:file/csv;base64,{b64}" download="captura.csv" target="_blank">'
        'Скачать как CSV</a>'
    )


services = [
    'Комплекс организационных, технических...',
    'Проверка маркировки цепей узла учета ...',
    'Оценка выполнения клиентом правил экс...',
    '"ПАКЕТ ""РЕКОНСТРУКЦИЯ МНОГОТАРИФНОГО...',
    'Услуги по энергоменеджменту',
]


st.set_page_config(layout="wide")

st.title("Energy & Brains")
st.subheader("Список контрагентов для предложения услуги")

service_name = st.sidebar.selectbox("Выберите услугу для ранжирования", services)
service_id = services.index(service_name)

defaults = {
    services[0]: ["до 3 лет", "мелкая", "1-10 млн"],
    services[1]: ["до 5 лет", "средняя", "10-100млн"],
    services[2]: ["старше 5 лет", "крупная", ">100"],
    services[3]: ["старше 5 лет", "крупная", ">100"],
    services[4]: ["старше 5 лет", "крупная", ">100"],
}[service_name]

st.sidebar.subheader("Карточка контрагента")
st.sidebar.select_slider(
    "Возраст организации",
    ["до года", "до 3 лет", "до 5 лет", "старше 5 лет"],
    defaults[0],
)
st.sidebar.select_slider(
    "Масштаб деятельности",
    ["мелкая", "средняя", "крупная"],
    defaults[1],
)
st.sidebar.select_slider(
    "Выручка",
    ["до 1 млн", "1-10 млн", "10-100млн", ">100"],
    defaults[2],
)

st.sidebar.subheader("Прибыль")
st.sidebar.select_slider(
    "Стоимость",
    ["до 1 млн", "1-10 млн", "10-100млн", ">100"],
    defaults[2],
)
st.sidebar.select_slider(
    "Уставной капитал",
    ["до 1 млн", "1-10 млн", "10-100млн", ">100"],
    defaults[2],
)
st.sidebar.subheader("Судебные дела")
st.sidebar.select_slider(
    "Сейчас рассматривается дел - всего",
    [0, 1, 5, 10],
)
st.sidebar.select_slider(
    "Сейчас рассматривается дел – ИнтерРао",
    [0, 1, 5, 10],
)
st.sidebar.select_slider(
    "Сумма иска – всего",
    ["до 1 млн", "1-10 млн", "10-100млн", ">100"],
    defaults[2],
)
st.sidebar.select_slider(
    "Сумма иска – ИнтерРао",
    ["до 1 млн", "1-10 млн", "10-100млн", ">100"],
    defaults[2],
)

st.sidebar.subheader("Анализ энергопотребления")
st.sidebar.select_slider("Финансовое состояние", ["A", "AA", "AA+"])
st.sidebar.select_slider("Разрешенная мощность", ["СН2", "НН", "ВН", "СН1", "ГН"])
st.sidebar.select_slider("Полезный отпуск", ["<1", "1-10", "10-100", "100+"])
st.sidebar.select_slider("Метод расчета", ["любой", "потребление", "потребление+мощность"],
                         "потребление+мощность")
st.sidebar.slider("Число часов использования максимальной мощности", 1, 24)

st.sidebar.subheader("Клиентский опыт")
st.sidebar.checkbox("Пользование платными услугами")
st.sidebar.checkbox("Наличие ЛК")
st.sidebar.checkbox("Отсутствие задолженность 3 месяца подряд")
st.sidebar.checkbox("Отсутствие задолженность 6 месяцев подряд и более")
st.sidebar.checkbox("Наличие претензий/жалоб за период")
st.sidebar.checkbox("Подача своевременно показаний (при необходимости)")
st.sidebar.checkbox("Аванс и предоплаты")

company_filter = st.text_input("Поиск компаний")

if company_filter:
    df = ch_query("""
    SELECT id, okved_name, score
    FROM po_yur_dannie_po_dogovoru
    WHERE id::String ILIKE {company_filter:String}
    JOIN (SELECT id, score FROM predicts_v1
    WHERE service_id = {service_id:Int32}
    ORDER BY score DESC
    LIMIT 10) t USING id
    ORDER BY score DESC
    """, {
        "service_id": service_id,
        "company_filter": f"%{company_filter}%"
    })
else:
    df = ch_query("""
    SELECT id, okved_name, score
    FROM po_yur_dannie_po_dogovoru
    JOIN (SELECT id, score FROM predicts_v1
    WHERE service_id = {service_id:Int32}
    ORDER BY score DESC
    LIMIT 10) t USING id
    ORDER BY score DESC
    """, {
        "service_id": service_id,
    })


df['okved_name'] = df['okved_name'].str.slice(0, 30)
st.table(df)


st.markdown(as_csv(df), unsafe_allow_html=True)

selected_company = st.selectbox("Просмотр компании", df.id)
st.subheader("Интерпретация score модели")
st.subheader("Информация по контрагенту")

df2 = ch_query("""
    SELECT service_name, pay_sum_with_nds
    FROM suvk_yur_platnie_nachisleniya_i_oplati
    WHERE id = {id:Int32}
    """, {"id": selected_company})
if len(df2):
    st.table(df2)
else:
    st.text("Нет данных по платным услугам")

df3 = ch_query("""
SELECT toStartOfMonth(registration_date)::String dt, sum(cost_of_taken_energy_with_nds_rub) cost
FROM po_yur_poleznii_otpusk
WHERE id = {id:Int32}
GROUP BY registration_date
ORDER BY dt
""", {"id": selected_company}).set_index("dt")

if len(df3):
    st.line_chart(df3)
else:
    st.text("Нет данных по полезному отпуску")
