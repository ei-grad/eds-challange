from typing import Dict, Union

import requests
import lzma
from pyarrow import parquet as pq
from io import BytesIO
import json


class ClickhouseError(RuntimeError):
    pass


ch_creds = json.load(open("ch_creds.json"))


def ch_query(query, params=None):
    """Send SELECT query to clickhouse and return result as pandas.DataFrame.

    Example:

    >>> ch_query("SELECT * FROM table WHERE int_column = {id:UInt8} and string_column = {phrase:String}",
    ...          params={"id": 1, "phrase": "Hello, world!"})
    """
    request_params: Dict[str, Union[str, bytes, int, float]] = {
        "query": query,
        "enable_http_compression": 1,
    }
    if ch_creds.get("database") is not None:
        request_params["database"] = ch_creds["database"]
    if params is not None:
        for k, v in params.items():
            request_params[f"param_{k}"] = v
    headers = {
        "Accept-Encoding": "xz",
        "X-ClickHouse-Format": "Parquet",
    }
    if ch_creds.get("user") is not None:
        headers["X-ClickHouse-User"] = ch_creds["user"]
    if ch_creds.get("password") is not None:
        headers["X-ClickHouse-Key"] = ch_creds["password"]
    response = requests.post(ch_creds["url"], params=request_params, headers=headers)
    data = response.content
    if response.headers.get("Content-Encoding") == "xz":
        data = lzma.decompress(data)
    if response.status_code == 200:
        df = pq.read_table(BytesIO(data)).to_pandas()
        for col, dtype in df.dtypes.items():
            if len(df[col]) > 0 and \
                    dtype == object and \
                    isinstance(df[col][0], bytes):  # Only process object columns.
                try:
                    # decode, or return original value if decode return Nan
                    df[col] = df[col].str.decode('utf-8').fillna(df[col])
                except UnicodeDecodeError:
                    pass
        return df
    raise ClickhouseError("got %d response: %s" % (response.status_code, data.decode("utf-8")))
